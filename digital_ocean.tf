variable "do_token" {}
variable "aws_access_key" {}
variable "aws_secret_key" {}
variable "abuango_pub_key" {}

# Configure the DigitalOcean Provider
provider "digitalocean" {
  token = "${var.do_token}"
}


data "terraform_remote_state" "network" {
  backend = "s3"
  config {
    bucket = "abuango-terraform"
    key    = "terraform.tfstate"
    access_key = "${var.aws_access_key}"
    secret_key = "${var.aws_secret_key}"
    region = "eu-west-2"
  }
}

resource "digitalocean_ssh_key" "digitalocean_ssh_key" {
  name = "Abubakar Ango-Mac"
  public_key = "${var.abuango_pub_key}"
}


# Create a web server
resource "digitalocean_droplet" "web1" {
  ssh_keys           = [
    "${digitalocean_ssh_key.digitalocean_ssh_key.id}"
  ]
  image              = "ubuntu-16-04-x64"
  region             = "lon1"
  size               = "2gb"
  private_networking = true
  backups            = true
  name               = "web1"
  provisioner "remote-exec" {
      connection {
        type     = "ssh"
        user     = "root"
        private_key = "${file("private.key")}"
    }
    inline = [
     "sudo apt-get update", 
     "apt-get -y install apache2",
     "echo '<h1>Hello Digital Ocean Lagos</h1>' > /var/www/html/hello.html"
    ]
  }
}

output "Public ip web1: " {
  value = "${digitalocean_droplet.web1.ipv4_address}"
}

output "Private ip web1: " {
  value = "${digitalocean_droplet.web1.ipv4_address_private}"
}

output "Price per Monthd for web1: " {
    value = "${digitalocean_droplet.web1.price_monthly}"
}

# Create a web server
resource "digitalocean_droplet" "web-02" {
  ssh_keys           = [
    "${digitalocean_ssh_key.digitalocean_ssh_key.id}"
  ]
  image              = "ubuntu-16-04-x64"
  region             = "lon1"
  size               = "2gb"
  private_networking = true
  backups            = true
  name               = "web-02"
  provisioner "remote-exec" {
    connection {
        type     = "ssh"
        user     = "root"
        private_key = "${file("~/.ssh/id_rsa")}"
    }
    inline = [
     "sudo apt-get update", 
     "apt-get -y install apache2",
     "echo '<h1>Hello Digital Ocean Lagos</h1>' > /var/www/html/hello.html"
    ]
  }
}

output "Public ip web-02: " {
  value = "${digitalocean_droplet.web-02.ipv4_address}"
}

output "Private ip web-02: " {
  value = "${digitalocean_droplet.web-02.ipv4_address_private}"
}

output "Price per Monthd for web-02: " {
    value = "${digitalocean_droplet.web-02.price_monthly}"
}


resource "digitalocean_loadbalancer" "publiclb" {
  name = "publiclb"
  region = "lon1"

  forwarding_rule {
    entry_port = 80
    entry_protocol = "http"

    target_port = 80
    target_protocol = "http"
  }

  healthcheck {
    port = 22
    protocol = "tcp"
  }

  droplet_ids = ["${digitalocean_droplet.web1.id}", "${digitalocean_droplet.web-02.id}"]
}

output "Load Balancer IP: " {
    value = "${digitalocean_loadbalancer.publiclb.ip}"
}